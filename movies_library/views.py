from django.http import HttpResponseRedirect
from django.shortcuts import render,redirect
from django.http import HttpResponse
from movies_library.models import *
from movies_library.parse import *

def index(request):
    if(request.GET.get('clear')):
        ClearTable(request.GET.get('clear'))
        return HttpResponse('Success')
    if(request.POST.get('remove')):
        RemoveField(request.POST.get('active'), request.POST.get('id'))
        return HttpResponseRedirect('lib/?active=%s'%request.POST.get('active'))
    return render(request,'index.html',GetContext(request))


def addField(request):
    if (not request.GET.get('table') and not request.POST.get('elem_table')):
        return HttpResponse('Missing table parameter' + str(dict(request.POST.iterlists())) + str(dict(request.GET.iterlists())))
    if request.POST.get('elem_table'):
        act = request.POST.get('elem_table')
        AddModelField(request.POST)
        return HttpResponseRedirect('lib/?active=%s'%request.GET.get('table'))
    return render(request,'add.html',{
        'col_list':GetFieldInputForm(request.GET.get('table')),
        'table':request.GET.get('table')
    })

# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-11-29 09:58
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Actor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('bidth_date', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='Directors',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('birth_date', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='Genre',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Movie',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('duration', models.TimeField()),
                ('actors', models.ManyToManyField(to='movies_library.Actor')),
                ('director', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='movies_library.Directors')),
                ('genre', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='movies_library.Genre')),
            ],
        ),
        migrations.CreateModel(
            name='Studio',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
            ],
        ),
        migrations.AddField(
            model_name='movie',
            name='studio',
            field=models.ManyToManyField(to='movies_library.Studio'),
        ),
        migrations.AddField(
            model_name='directors',
            name='studios',
            field=models.ManyToManyField(to='movies_library.Studio'),
        ),
    ]

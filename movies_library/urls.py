from django.conf.urls import url
from django.contrib import admin
import movies_library.views as views

urlpatterns = [
    url(r'add',views.addField),
    url(r'^$', views.index),
]
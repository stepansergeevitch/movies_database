from __future__ import unicode_literals
import django.apps
from django.forms import model_to_dict
from django.db import models
from dateutil import parser
import re


class Genres(models.Model):
    name = models.CharField(max_length=200)

    @staticmethod
    def getHeaders():
        return ['Id','Name']


class Actors(models.Model):
    name = models.CharField(max_length=200)
    birth_date = models.DateField()

    @staticmethod
    def getHeaders():
        return ['Id','Name','Birth_Date']


class Studios(models.Model):
    name = models.CharField(max_length=200)

    @staticmethod
    def getHeaders():
        return ['Id','Name']


class Directors(models.Model):
    name = models.CharField(max_length=200)
    birth_date = models.DateField()

    @staticmethod
    def getHeaders():
        return ['Id','Name','Birth_Date']


class Movies(models.Model):
    name = models.CharField(max_length=200)
    duration = models.TimeField()
    genres = models.ManyToManyField(Genres)
    director = models.ForeignKey(Directors)
    actors = models.ManyToManyField(Actors)
    studio = models.ForeignKey(Studios)

    @staticmethod
    def getHeaders():
        return ['Id','Name','Duration','Genre','Directors','Actors','Studio']

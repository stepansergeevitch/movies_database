from movies_library.models import *
from django.db import connection as c

# directors films
# studios films
# actors studios

queries = ['select * from movies_library_movies left join movies_library_directors on movies_library_movies.director_id=\
            movies_library_directors.id where movies_library_directors.name = %s',
           'select * from movies_library_movies left join movies_library_studios on movies_library_movies.studio_id=\
            movies_library_studios.id where movies_library_studios.name = %s',
           'select * from movies_library_studios where movies_library_studios.id in\
           (select movies_library_movies.studio_id from movies_library_movies where movies_library_movies.id in \
           (select movies_library_movies_actors.movies_id from movies_library_movies_actors inner join movies_library_actors\
           on movies_library_movies_actors.actors_id = movies_library_actors.id where movies_library_actors.name = %s))',
           'select * from movies_library_genres where movies_library_genres.id in\
           (select movies_library_movies_genres.genres_id from movies_library_movies_genres where movies_library_movies_genres.movies_id in \
           (select movies_library_movies_actors.movies_id from movies_library_movies_actors inner join movies_library_actors\
           on movies_library_movies_actors.actors_id = movies_library_actors.id where movies_library_actors.name = %s))',
           'select * from movies_library_directors where movies_library_directors.id in \
           (select movies_library_movies.director_id from movies_library_movies where movies_library_movies.studio_id in \
           (select movies_library_studios.id from movies_library_studios where movies_library_studios.name = %s))'
           ]

# films with same or less actors
# films of same genre
# directors with same genres

_models = [Movies,Movies,Studios,Genres,Directors]

set_queries = ['select * from movies_library_movies where not(movies_library_movies.name = %s) and not exists  (select *  from movies_library_movies_actors \
                where movies_library_movies_actors.movies_id = movies_library_movies.id and movies_library_movies_actors.actors_id \
                not in (select movies_library_movies_actors.actors_id from movies_library_movies_actors inner join movies_library_movies\
                on movies_library_movies_actors.movies_id = movies_library_movies.id where movies_library_movies.name = %s))']

set_models = [Movies]


def GetHeadersNames(m):
    def get_w(f):
        if f.name == 'id':
            return 0
        if type(f) in [models.ForeignKey,models.ManyToOneRel]:
            return 2
        if type(f) in [models.ManyToManyField,models.ManyToManyRel]:
            return 3
        return 1

    return [j.name for j in sorted(m._meta.get_fields(),key=get_w) if type(j) != models.ManyToOneRel]

def GetFieldsTitleNames(m):
    return [i.replace('_',' ').title() for i in GetHeadersNames(m)]

def GetFields(m):
    def get_w(f):
        if f.name == 'id':
            return 0
        if type(f) in [models.ForeignKey,models.ManyToOneRel]:
            return 2
        if type(f) in [models.ManyToManyField,models.ManyToManyRel]:
            return 3
        return 1


    return [j for j in sorted(m._meta.get_fields(),key=get_w) if type(j) != models.ManyToOneRel]

def GetAttributeContainer(c):
    if type(c) == models.CharField:
        return '<input required type="text" class="form-control" name="%s">' % (c.name)
    if type(c) == models.DateField:
        return '<input required type="date" class="form-control" name="%s">' % (c.name)
    if type(c) == models.TimeField:
        return '<input required pattern="[0-9]{0,2}:[0-9]{0,2}:[0-9]{0,2}" class="form-control" name="%s">' % c.name
    if type(c) == models.ForeignKey:
        l = [i.name for i in c.rel.to.objects.all()]
        print(['<option value="%s">%s</option>'%(i,i) for i in l])
        options = ''.join(['<option value="%s">%s</option>'%(i,i) for i in l])
        return '<select class="form-control" required name="%s">%s</select>'%(c.name,options)
    if type(c) == models.ManyToManyField:
        l = [i.name for i in c.rel.to.objects.all()]
        print(['<option value="%s">%s</option>' % (i, i) for i in l])
        options = ''.join(['<option value="%s">%s</option>' % (i, i) for i in l])
        return '<select class="form-control" multiple="multiple" required name="%s">%s</select>' % (c.name, options)
    return '<input type="text">'

def GetFieldInputForm(name):
    l = []
    m = django.apps.apps.get_model('movies_library', name)
    v = [models.ManyToManyRel,models.ManyToOneRel]
    for i in m._meta.get_fields():
        if(i.name != 'id' and type(i) not in v):
            l += [{'name':i.name,'type':GetAttributeContainer(i)}]
    return l

def GetModelFields(m):
    data = [[] for i in range(len(m.objects.all()))]
    for i in range(len(m.objects.all())):
        for j in GetFields(m)[1:]:
             try:
                if(type(j) == models.ForeignKey):
                    s = ''
                    exec('s = m.objects.all()[i].%s.name' % j.name)
                    data[i] += [['text',[s]]]
                elif(type(j) == models.ManyToManyField ):
                    t = []
                    exec('t = [l.name for l in m.objects.all()[i].%s.all()]' % j.name)
                    data[i] += [['list',t]]
                elif(type(j) == models.ManyToManyRel):
                    t = []
                    exec('t = [l.name for l in m.objects.all()[i].%s_set.all()]' % j.name)
                    data[i] += [['list', t]]
                else:
                    s = ''
                    exec('s = m.objects.all()[i].%s' % j.name)
                    data[i] += [['text',[s]]]
             except:
                 data[i] += [['text',["--"]]]
    return [[m.objects.all()[i].id,data[i]] for i in range(len(data))]

def GetModelContext(request):
    name = request.GET.get('active')
    m = django.apps.apps.get_model('movies_library',name)
    return {'headers':GetFieldsTitleNames(m), 'elements':GetModelFields(m)}

def CreateModelField(data, m):
    _data = {}
    m_d = {}
    for i in data:
        t = m._meta.get_field(i)
        v = [models.ManyToManyField, models.ForeignKey]
        if(type(t) in v):
            l = []
            for j in data[i]:
                l += [t.rel.to.objects.get(name=j)]
            if type(t) == models.ForeignKey:
                _data[i] = l[0]
            else:
                m_d[i] = l
        else:
            _data[i] = data[i][0]
    t = m.objects.create(**_data)
    for i in m_d:
        for j in m_d[i]:
            eval('t.%s.add(j)'%(i))
    print(t.__dict__)
    t.save()

def AddModelField(data):
    data = dict(data.iterlists())
    t = str(data.get('elem_table')[-1])
    m = django.apps.apps.get_model('movies_library',t)
    data.pop('elem_table',None)
    data = dict([(i,data[i]) for i in data])
    data.pop('csrfmiddlewaretoken',None)
    CreateModelField(data, m)

def ClearTable(name):
    m = django.apps.apps.get_model('movies_library', name)
    m.objects.all().delete()

def RemoveField(table, id):
    django.apps.apps.get_model('movies_library', table).objects.get(id=id).delete()

def GetQueryFields(query,name,m):
    t = c.cursor().execute(query, name)
    obj = t.fetchall()
    data = []
    for i in range(len(obj)):
        data += [[]]
        k = 0
        for j in GetFields(m)[1:]:
            k += 1
            try:
                if (type(j) == models.ForeignKey):
                    s = j.rel.to.objects.filter(id=obj[i][k])[0].name
                    data[i] += [['text', [s]]]
                elif (type(j) == models.ManyToManyField):
                    t = m.objects.filter(id=obj[i][0])[0]
                    exec('t = [l.name for l in t.%s.all()]' % j.name)
                    data[i] += [['list', t]]
                elif (type(j) == models.ManyToManyRel):
                    t = m.objects.filter(id=obj[i][0])[0]
                    exec('t = [l.name for l in t.%s_set.all()]' % j.name)
                    data[i] += [['list', t]]
                else:
                    data[i] += [['text', [obj[i][k]]]]
            except:
                data[i] += [['text', ["--"]]]
    return [[obj[i][0],data[i]] for i in range(len(data))]

def GetQueryContext(request):
    query = request.GET.get('query')
    if request.GET.get('name'):
        name = request.GET.get('name')
        ind = int(query) - 1
        m = _models[ind]
        return {'q_name': name, 'headers': GetFieldsTitleNames(m),
                'elements': GetQueryFields(queries[ind],[name],m)}
    return {}

def GetSetQueryContext(request):
    set_query = request.GET.get('set-query')
    if request.GET.get('name'):
        name = request.GET.get('name')
        ind = int(set_query) - 1
        m = set_models[ind]
        return {'q_name': name, 'headers': GetFieldsTitleNames(m),
                'elements': GetQueryFields(set_queries[ind], [name,name], m)}
    return {}


def GetContext(request):
    if request.GET.get('query'):
        query = request.GET.get('query')
        return dict({'active': '', 'query': query, 'set_query': ''},**GetQueryContext(request))
    if request.GET.get('set-query'):
        set_query = request.GET.get('set-query')
        return dict({'active': '', 'query':'', 'set_query': set_query},**GetSetQueryContext(request))
    if request.GET.get('active'):
        active = request.GET.get('active')
        return dict({'active':active,'query':'','set_query':''},**GetModelContext(request))
    return {'active': '', 'query':'', 'set_query': ''}
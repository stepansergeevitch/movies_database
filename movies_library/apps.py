from django.apps import AppConfig


class MoviesLibraryConfig(AppConfig):
    name = 'movies_library'
